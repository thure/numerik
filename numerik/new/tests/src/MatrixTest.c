#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../../library/src/Tuple.h"
#include "../../library/src/Vector.h"
#include "../../library/src/Matrix.h"


int main (void) {

  const unsigned int   n = 10;

  MatrixOfDouble  A; /* Abbildungsmatrix */

  VectorOfDouble  v = VectorOfDouble_Create (n); /* Eingabevektor */

  VectorOfDouble  r = VectorOfDouble_Create (n); /* Ergebnisvektor */

  char  filename[]  = "../inputs/permut.txt";

  int   i = -1; /* Zaehler */

  A = MatrixOfDouble_LoadFromFile (filename);

  printf ("Permutationsmatrix A =\n\n"); MatrixOfDouble_Print (A);
  printf ("\n\n");

  for (i = 1; i <= 10; i++) {
    VectorOfDouble_SetEntry_1 (v, i, i);
  }

  printf ("v = "); VectorOfDouble_Print (v); printf ("\n\n");

  MatrixVectorMult_inPlace (r, A, v); /*     r := A * v     */

  printf ("r = "); VectorOfDouble_Print (r); printf ("\n\n");

  VectorOfDouble_Destroy (&r);
  VectorOfDouble_Destroy (&v);
  MatrixOfDouble_Destroy (A);
  
  return EXIT_SUCCESS;

}
