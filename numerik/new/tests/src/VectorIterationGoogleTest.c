/* Mathematik C fuer Informatikstudierende -- Serie 7 -- 15.12.2013
   Anna Schleimer, Thure Duehrsen
   Aufgabe 7.3: Vektoriteration */


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../../library/src/Tuple.h"
#include "../../library/src/Vector.h"
#include "../../library/src/Matrix.h"

void PageRank_g_A (VectorOfDouble x_neu, MatrixOfDouble A,
		   VectorOfDouble x,     double c);
/* Vorausdeklaration, weil der Compiler sonst meckert */

int main (int argc, char* argv[]) {

  /* Einziger auf der Kommandozeile zu uebergebender Parameter ist c,
     der Anteil der Basiswichtigkeit */

  /* const */ double         c;

  const unsigned int   n = 6;

  MatrixOfDouble       A; /* Abbildungsmatrix */

  VectorOfDouble       w0; /* Startvektor */

  VectorOfDouble       w_i_alt; /* Vektor zur Iteration i-1 */

  VectorOfDouble       w_i_neu; /* Vektor zur Iteration i */

  VectorOfDouble       diff; /* w_i_neu - w_i_alt */

  double               norm_i_diff; /* Norm des Vektors diff */

  const double         eps = 0.0001;

  const char           filename[] = "../inputs/linkmatrix.txt";
                       /* Speicherort der Abbildungsmatrix */

  int                  i = -1; /* Zaehler */


  if (argc != 2) {
    fprintf (stderr, "Es muss genau ein Argument uebergeben werden:"
             "die Basiswichtigkeit als double-Zahl\n");
    exit(1);
  }

  c = atof (argv[1]);  /* Hier fehlt Fehlerbehandlung */

  printf ("\nFixpunktiteration zu c = %0.3f\n\n", c);

  A = MatrixOfDouble_LoadFromFile (filename);

  printf ("Linkmatrix A =\n\n"); MatrixOfDouble_Print (A);
  printf ("\n\n");

  w0 = VectorOfDouble_Create (6); /* w0 \in double^6 */

  /* w0 soll der erste Standardbasisvektor sein */
  for (i = 1; i<=6; i++) {

    if (i == 1)
      VectorOfDouble_SetEntry_1 (w0, i, 1);
    else
      VectorOfDouble_SetEntry_1 (w0, i, 0);

  }

  printf ("w0 = "); VectorOfDouble_Print (w0); printf ("\n\n");

  w_i_neu = VectorOfDouble_Copy (w0);
  /* jetzt ist w_i_neu auch der erste Standardbasisvektor, und wir
     koennen ihn durch die Iterationen schicken.  w_i_neu bekommt das
     Ergebnis jeder Iteration zugewiesen
  */

  w_i_alt = VectorOfDouble_Create (n);
  /* bewahrt die alte Iteration auf */

  diff = VectorOfDouble_Create (n);
  /* bewahrt die Differenz zweier aufeinanderfolgender Iterationen auf */

  i = 0;
  do {

    i++;

    /* w_i_alt <--- w_i_neu --- alte Iteration retten */
    VectorOfDouble_CopyValues(w_i_alt, w_i_neu);

    /* Eine Iteration durchfuehren: w_i_neu <--- g_A(w_i_neu) */
    PageRank_g_A(w_i_neu, A, w_i_alt, c);

    /* Norm des Differenzvektors bestimmen */
    /* diff = w_i_neu - w_i_alt */
    VectorOfDouble_Sub (diff, w_i_neu, w_i_alt);
    norm_i_diff = VectorOfDouble_Norm (diff);

    printf ("w_%3d = ",i); VectorOfDouble_Print (w_i_neu); printf ("\n");

    } while (norm_i_diff >= eps);


  VectorOfDouble_Destroy (&diff);
  VectorOfDouble_Destroy (&w_i_alt);
  VectorOfDouble_Destroy (&w_i_neu);
  VectorOfDouble_Destroy (&w0);
  MatrixOfDouble_Destroy (A);
  
  printf ("\n");

  return EXIT_SUCCESS;

}

void PageRank_g_A (VectorOfDouble x_neu, MatrixOfDouble A,
		   VectorOfDouble x,     double c) {

  /* Fuehrt einen Iterationsschritt der PageRank-Iteration durch. */

  unsigned int n = x.length; /* Laenge aller beteiligten Vektoren */

  VectorOfDouble basiswichtigkeit;
  /* Vektor der Basiswichtigkeit (VL 13.12.13) */

  unsigned int i; /* Eine Zaehlvariable kann man immer mal gebrauchen */

  if (c <= 0 || c >= 1) {

    printf ("Dieses c tut mir weh!\n");
    exit (1);

  }

  /* Hier muss noch mehr Fehlerbehandlung hin */

  /* benoetigte Vektoren erzeugen */
  basiswichtigkeit = VectorOfDouble_Create (n);

  /* basiswichtigkeit mit Einsen vorbesetzen */
  for (i = 1; i <= n; i++) {

    VectorOfDouble_SetEntry_1 (basiswichtigkeit, i, 1);

  }

  /* und skalieren */
  VectorOfDouble_Scale (basiswichtigkeit, c/n);

  MatrixVectorMult_inPlace (x_neu, A, x); /* x_neu <--- A * x;
					    ermittelt die eigentliche
                              		    Wichtigkeit, hat aber noch
                			    Fixpunkt 0 */

  VectorOfDouble_Scale (x_neu, 1-c);
  /* x_neu <--- (1-c) * x_neu; vermindert die eben berechnete
     Wichtigkeit ein wenig, damit die Basiswichtigkeit dazuaddiert
     werden kann */

  VectorOfDouble_Add (x_neu, x_neu, basiswichtigkeit);
  /* x_neu <--- x_neu + basiswichtigkeit */

  VectorOfDouble_Destroy (&basiswichtigkeit);

}
