#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "../../library/src/Tuple.h"
#include "../../library/src/Vector.h"
#include "../../library/src/Matrix.h"


int main (void) {

  MatrixOfDouble C;
  MatrixOfDouble A;
  MatrixOfDouble B;
  MatrixOfDouble D;

  unsigned int am = 4;
  unsigned int an = 6;
  unsigned int bm = 6;
  unsigned int bn = 8;

  unsigned int i = 0;
  unsigned int j = 0;

  A = MatrixOfDouble_Create (am, an, ROWWISE);
  B = MatrixOfDouble_Create (bm, bn, ROWWISE);
  C = MatrixOfDouble_Create (am, bn, ROWWISE);

  /* Matrix A fuellen */

  for (i = 1; i <= am; i++) {
    for (j = 1; j <= an; j++) {

      MatrixOfDouble_SetEntry_1 (A, i, j, ((signed)i)-((signed)j));

    }
  }
  
  printf ("Matrix A =\n\n"); MatrixOfDouble_Print (A);
  printf ("\n\n");

  /* Matrix B fuellen */

  for (i = 1; i <= bm; i++) {
    for (j = 1; j <= bn; j++) {

      MatrixOfDouble_SetEntry_1 (B, i, j, i+j);

    }
  }

  printf ("Matrix B =\n\n"); MatrixOfDouble_Print (B);
  printf ("\n\n");

  MatrixMatrixMult_inPlace (C, A, B);

  printf ("Matrix C =\n\n"); MatrixOfDouble_Print (C);
  printf ("\n\n");

  D = MatrixMatrixMult (A, B);

  printf ("Matrix D =\n\n"); MatrixOfDouble_Print (D);
  printf ("\n\n");
  
  MatrixOfDouble_Destroy (D);
  MatrixOfDouble_Destroy (C);
  MatrixOfDouble_Destroy (B);
  MatrixOfDouble_Destroy (A);

  return EXIT_SUCCESS;

}
