#include <stdlib.h>
#include <stdio.h>
#include "../../library/src/Tuple.h"

int main (void) {

  TupleOfDouble t;
  TupleOfDouble u;

  t = TupleOfDouble_Create (5);
  u = TupleOfDouble_Create (5);

  t[2] = u[3] = 42;

  printf ("%3.5f %3.5f %3.5f %3.5f %3.5f\n", t[0], t[1], t[2], t[3], t[4]);
  printf ("%3.5f %3.5f %3.5f %3.5f %3.5f\n", u[0], u[1], u[2], u[3], u[4]);

  TupleOfDouble_Destroy (t);
  TupleOfDouble_Destroy (u);

  return EXIT_SUCCESS;

}
