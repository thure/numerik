#include <stdlib.h>
#include <stdio.h>
#include "../../library/src/Vector.h"

int main (void) {

  unsigned int n = 6;
  unsigned int i;

  VectorOfDouble v;
  VectorOfDouble w;
  VectorOfDouble sum;

  v = VectorOfDouble_Create (n);
  w = VectorOfDouble_Create (n);
  sum = VectorOfDouble_Create (n);

  printf("v = "); VectorOfDouble_Print (v); printf ("\n\n");

  for (i = 1; i <= n; i++) {

    VectorOfDouble_SetEntry_1 (v, i, 2*i);
    VectorOfDouble_SetEntry_1 (w, i, 5);

  }

  printf("v = "); VectorOfDouble_Print (v); printf ("\n\n");
  printf("w = "); VectorOfDouble_Print (w); printf ("\n\n");

  VectorOfDouble_Add (v, v, w);

  printf("v = "); VectorOfDouble_Print (v); printf ("\n\n");

  VectorOfDouble_Normalise (v);

  printf("v_norm = "); VectorOfDouble_Print (v); printf ("\n\n");

  printf("sum = "); VectorOfDouble_Print (sum); printf ("\n\n");

  VectorOfDouble_Destroy (&v);
  VectorOfDouble_Destroy (&w);
  VectorOfDouble_Destroy (&sum);

  return EXIT_SUCCESS;

}
