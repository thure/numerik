#include <stdlib.h>
#include <stdio.h>
#include "../../library/src/Vector.h"
#include "../../library/src/Matrix.h"

int main (void) {

  unsigned int n = 4;

  VectorOfDouble v;
  /* MatrixOfDouble A; */

  v = VectorOfDouble_Create (n);

  printf ("\n");
  
  printf("v = "); VectorOfDouble_Print (v); printf ("\n\n");

  VectorOfDouble_SetEntry_1 (v, 1, 10);

  printf("v = "); VectorOfDouble_Print (v); printf ("\n\n");

  fprintf (stderr, "Vektor zerstoeren...\n");
  VectorOfDouble_Destroy (&v);

  if (v.values != NULL) {
    fprintf (stderr, "TEST: Vektor ist nicht zerstoert\n");
    fprintf (stderr, "TEST: Vektor.values zeigt auf %p\n\n", (void*)v.values);
    printf("v = "); VectorOfDouble_Print (v); printf ("\n\n");

  } else {
    
    fprintf (stderr, "TEST: Vektor ist zerstoert\n");
    fprintf (stderr, "TEST: Vektor.values zeigt auf %p\n\n", (void*)v.values);

    /* soll einen Laufzeitfehler ausloesen */
    printf("v = "); VectorOfDouble_Print (v); printf ("\n\n");
  }

  /*

  A = MatrixOfDouble_Create (n, n, ROWWISE);

  MatrixOfDouble_SetEntry_1 (A, 2, 2, 42);

  MatrixOfDouble_Print (A);

  MatrixOfDouble_PrintOrdering (A);

  fprintf (stderr, "Matrix zerstoeren...\n");
  MatrixOfDouble_Destroy (A);
  MatrixOfDouble_Print (A);
  if (A.values != NULL) {
    fprintf (stderr, "Matrix ist nicht zerstoert\n");
    fprintf (stderr, "Matrix.values zeigt auf %p\n", (void*)A.values);
  } else {
    fprintf (stderr, "Matrix ist zerstoert\n");
    fprintf (stderr, "Matrix.values zeigt auf %p\n", (void*)A.values);
  }

  */


  /* Fehlerhafte Zugriffe */
  
  /*
  
  VectorOfDouble_SetEntry_1 (v, 1, 42);
  printf("v = "); VectorOfDouble_Print (v); printf ("\n\n");
  MatrixOfDouble_SetEntry_1 (A, 1, 1, 42);
  MatrixOfDouble_Print (A);
  MatrixOfDouble_PrintOrdering (A);

  */

  return EXIT_SUCCESS;

}
