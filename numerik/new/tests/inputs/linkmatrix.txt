6 6

0.          0.          0.85        0.425       0.          0.        
0.425       0.          0.          0.          0.          0.        
0.          0.425       0.          0.          0.28333333  0.85      
0.425       0.425       0.          0.          0.28333333  0.        
0.          0.          0.          0.425       0.          0.        
0.          0.          0.          0.          0.28333333  0.        


% Comments are now only allowed at the end of the file.
% Precede comments with a percent sign.
% Empty lines are allowed.
% End-of-line character must be a single LF.

% erste Zahl in der ersten Zeile: Anzahl der Zeilen
% zweite Zahl in der ersten Zeile: Anzahl der Spalten
% danach zeilenweise, durch Leerzeichen getrennt,
% die Eintraege der Matrix als echte double-Zahlen,
% d.h. *nicht* als Ausdruecke, die ausgewertet einen
% double-Ausdruck ergeben.

% tdu, 2013-12-15
