#!/bin/bash

usage() {

    echo -e "\nUsage: $0"

}

echo

if [ 0 -ne "$#" ]
then
    usage
    exit 1
fi

cd tests/bin || { echo "cannot chdir to executables"; exit 3; }

for file in *.out; do
    if [ "$file" == "VectorIterationGoogleTest.out" ]; then
        continue
    fi
    echo "$file"
    valgrind "./${file}"  2>&1 | grep 'ERROR SUMMARY'
    echo; echo
done

echo VectorIterationGoogleTest.out
valgrind ./VectorIterationGoogleTest.out 0.3 2>&1 | grep 'ERROR SUMMARY'
echo
