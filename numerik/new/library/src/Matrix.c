#include <stdlib.h>
#include <stdio.h>
/* #include <strings.h> */
#include "Tuple.h"
#include "Vector.h"
#include "Matrix.h"

MatrixOfDouble MatrixOfDouble_Create  (unsigned int rows,
                                       unsigned int cols,
				       MatrixOrdering ordering) {

  /* Bekommt zwei natuerliche Zahlen rows und cols, die fuer die
     Anzahl der Zeilen bzw. der Spalten stehen, und fordert
     ausreichend Speicher fuer m * n Double-Werte an. Alle Eintraege
     werden mit -1 vorbesetzt.
  */

  MatrixOfDouble result;

  result.numberOfRows = rows;
  result.numberOfColumns = cols;
  result.ordering = ordering;
  
  if (rows <= 0) {

    fprintf (stderr, "Invalid matrix initialiser:"
             " Number of rows is not positive\n");
    result.values = NULL;
    return result;

  }

  if (cols <= 0) {

    fprintf (stderr, "Invalid matrix initialiser:"
             " Number of columns is not positive\n");
    result.values = NULL;
    return result;

  }

  result.values = TupleOfDouble_Create (rows*cols);

  if (result.values == NULL) {

    fprintf (stderr, "Unable to create matrix: Nonexistent entries\n");
    
  }

  return result;
  

}







void             MatrixOfDouble_CopyValues  (MatrixOfDouble dest,
					     MatrixOfDouble src) {

  /* Performs dest = src, component-wise. */

  unsigned int i, j;

  if (src.values == NULL) {

    fprintf (stderr, "MatrixOfDouble_CopyValues: Matrix to copy is NULL\n");
    exit (1);

  }

  if (dest.values == NULL) {

    fprintf (stderr, "MatrixOfDouble_CopyValues:"
	     " Matrix to hold result is NULL\n");
    exit (1);

  }

  if (src.ordering != dest.ordering) {

    fprintf (stderr, "MatrixOfDouble_CopyValues:"
	     " Matrices do not have the same ordering\n");
    exit (1);

  }

  if (src.numberOfRows != dest.numberOfRows) {

    fprintf (stderr, "MatrixOfDouble_CopyValues:"
	     " Matrices do not have the same number of rows\n");
    exit (1);

  }

  if (src.numberOfColumns != dest.numberOfColumns) {

    fprintf (stderr, "MatrixOfDouble_CopyValues:"
	     " Matrices do not have the same number of columns\n");
    exit (1);

  }

  for (i = 1; i <= dest.numberOfRows; i++) {

    for (j = 1; j <= dest.numberOfColumns; j++) {

      MatrixOfDouble_SetEntry_1
	(dest, i, j, MatrixOfDouble_GetEntry_1(src, i, j));

    }
  
  }

}







MatrixOfDouble   MatrixOfDouble_Copy (MatrixOfDouble src) {

  /* Performs dest = src, component-wise. */

  MatrixOfDouble dest;
  dest = MatrixOfDouble_Create (src.numberOfRows,
				src.numberOfColumns,
				src.ordering);

  if (src.values == NULL) {

    fprintf (stderr, "VectorOfDouble_Copy: Vector to copy is NULL\n");
    exit (1);

  }

  MatrixOfDouble_CopyValues (dest, src);

  return dest;

}







void           MatrixOfDouble_Destroy (MatrixOfDouble A) {

  A.numberOfRows    = 0;
  A.numberOfColumns = 0;
  A.ordering        = NONE;

  TupleOfDouble_Destroy (A.values);
  A.values = NULL;

}







MatrixOfDouble   MatrixOfDouble_LoadFromFile (const char* filename) {

  FILE* F = NULL;

  MatrixOfDouble result;

  unsigned int numberOfRows = -1;
  unsigned int numberOfColumns = -1;
  /* halten die Anzahl der Zeilen und Spalten fest, um die Matrix
     erzeugen zu koennen */

  unsigned int i, j; /* Schleifenzaehler */

  double entry; /* bewahrt einen aus der Datei eingelesenen
		   Matrixeintrag auf, damit er leicht in die Matrix
		   eingebaut werden kann */

  int fscanf_return_value = -42; /* bewahrt den Rueckgabewert von fscanf zur
				    Auswertung auf*/
  
  unsigned int z = 0 ; /* zaehlt die Anzahl der erfolgreich eingelesenen
			  Matrixeintraege */

  MatrixOrdering ordering = ROWWISE; /* diese Funktion bastelt immer
					eine zeilenweise organisierte
					Matrix */
  
  F = fopen (filename, "r");

  if (F == NULL) {

    fprintf (stderr, "MatrixOfDouble_LoadFromFile"
	     ": Unable to read file %s\n", filename);
    fclose (F);
    exit (1);

  }

  fscanf (F, "%u", &numberOfRows);
  /* hier fehlt Fehlerbehandlung */

  fscanf (F, "%u", &numberOfColumns);
  /* hier fehlt Fehlerbehandlung */

  result = MatrixOfDouble_Create (numberOfRows, numberOfColumns, ordering);

  for (i = 1; i <= numberOfRows; i++) {

    for (j = 1; j <= numberOfColumns; j++) {

      fscanf_return_value = fscanf (F, "%lf", &entry);

      z = z + fscanf_return_value;

      MatrixOfDouble_SetEntry_1 (result, i, j, entry);

    }

  }

  for (i = 0; i < numberOfRows * numberOfColumns; i++) {

    fscanf (F, "%lf", &result.values[i]);
  }
  
  fclose (F);

  if (z < numberOfRows * numberOfColumns) {

    fprintf (stderr, "MatrixOfDouble_LoadFromFile"
  	     ": Es konnten nur %u der %u Eintraege"
  	     " der Matrix gelesen werden!\n",
  	     z, numberOfRows * numberOfColumns);
    fclose (F);
    exit (1);

  }
  
  return result;
  

}







void           MatrixOfDouble_Print (MatrixOfDouble A) {
  /* aus v03 */

  /* TODO
     Zahlenformat flexibel gestalten
  */
  
  unsigned int i; /* Iteriert ueber die Zeilen */
  unsigned int j; /* Iteriert ueber die Spalten */

  if (A.values == NULL) {

    fprintf (stderr, "MatrixOfDouble_Print: Nonexistent matrix\n");
    exit (1);

  }

  for (i = 1; i <= A.numberOfRows; i++) {

    printf ("[");

    for (j = 1; j < A.numberOfColumns; j++) {

      printf ("% 3.8f,  ", MatrixOfDouble_GetEntry_1 (A, i,j));

    }

    printf ("% 3.8f", MatrixOfDouble_GetEntry_1 (A, i, A.numberOfColumns));

    printf ("]\n");

  }

}







MatrixOrdering   MatrixOfDouble_getOrdering (MatrixOfDouble A) {

  if (A.values == NULL) {

    fprintf (stderr, "MatrixOfDouble_GetOrdering: Nonexistent matrix\n");
    exit (1);

  }

  return A.ordering;

}







void             MatrixOfDouble_setOrdering (MatrixOfDouble A,
					     MatrixOrdering ordering) {

  if (A.values == NULL) {

    fprintf (stderr, "MatrixOfDouble_SetOrdering: Nonexistent matrix\n");
    exit (1);

  }

  /* hier muss evtl. noch mehr Fehlerbehandlung hin.

     Aber das hier kann man auch zum Transponieren von Matrizen benutzen!!!!
     Glaube ich zumindest...
  */ 

  A.ordering = ordering;

}







void             MatrixOfDouble_PrintOrdering (MatrixOfDouble A) {

  if (A.values == NULL) {

    fprintf (stderr, "MatrixOfDouble_GetOrdering: Nonexistent matrix\n");
    exit (1);

  }

  switch (A.ordering) {
    
  case NONE:          printf ("none");          break;
  case ROWWISE:       printf ("rowwise");       break;
  case COLUMNWISE:    printf ("columnwise");    break;
    
  default:            printf ("BOOM");          break;
    
  }

}







double           MatrixOfDouble_GetEntry_0 (MatrixOfDouble A,
					    unsigned int i,
					    unsigned int j) {

  unsigned long int longIndex = 0;

  if (i+1 > A.numberOfRows) {

    fprintf (stderr, "MatrixOfDouble_GetEntry: Row index out of bounds\n");
    exit (1);

  }

  if (j+1 > A.numberOfColumns) {

    fprintf (stderr, "MatrixOfDouble_GetEntry: Column index out of bounds\n");
    exit (1);

  }

  if (A.values == NULL) {

    fprintf (stderr, "MatrixOfDouble_GetEntry: Nonexistent matrix\n");
    exit (1);

  }

  longIndex = i * A.numberOfColumns + j;

  return A.values[longIndex];
  
}







double           MatrixOfDouble_GetEntry_1 (MatrixOfDouble A,
					    unsigned int i,
					    unsigned int j) {

  return MatrixOfDouble_GetEntry_0 (A, i-1, j-1);

}







void             MatrixOfDouble_SetEntry_0 (MatrixOfDouble A,
					    unsigned int i,
					    unsigned int j,
					    double x) {

  MatrixOfDouble_SetEntry_1 (A, i+1, j+1, x);
  
}







void             MatrixOfDouble_SetEntry_1 (MatrixOfDouble A,
					    unsigned int i,
					    unsigned int j,
					    double x) {

  unsigned long int longIndex = 0;

  if (i == 0 || i > A.numberOfRows) {

    fprintf (stderr, "MatrixOfDouble_SetEntry: Row index out of bounds\n");
    exit (1);

  }

  if (j == 0 || j > A.numberOfColumns) {

    fprintf (stderr, "MatrixOfDouble_SetEntry: Column index out of bounds\n");
    exit (1);

  }

  if (A.values == NULL) {

    fprintf (stderr, "MatrixOfDouble_SetEntry: Nonexistent matrix\n");
    exit (1);

  }

  longIndex = (i-1) * A.numberOfColumns + (j-1);

  A.values[longIndex] = x;

}







void             MatrixOfDouble_Add (MatrixOfDouble dest,
                                     MatrixOfDouble addend,
                                     MatrixOfDouble augend) {

  unsigned int i = 0; /* Iteriert ueber die Zeilen von dest */
  unsigned int j = 0; /* Iteriert ueber die Spalten von dest */

  if (dest.values == NULL) {
    fprintf (stderr, "MatrixOfDouble_Add: Result matrix is NULL\n");
    exit(1);
  }
  
  if (addend.values == NULL) {
    fprintf (stderr, "MatrixOfDouble_Add: Result matrix is NULL\n");
    exit(1);
  }
  
  if (augend.values == NULL) {
    fprintf (stderr, "MatrixOfDouble_Add: Result matrix is NULL\n");
    exit(1);
  }
   
  if (dest.numberOfRows != addend.numberOfRows) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "dest.numberOfRows != addend.numberOfRows\n");
    exit(1);
  }

  if (dest.numberOfRows != augend.numberOfRows) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "dest.numberOfRows != augend.numberOfRows\n");
    exit(1);
  }

  if (augend.numberOfRows != addend.numberOfRows) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "augend.numberOfRows != addend.numberOfRows\n");
    exit(1);
  }

  if (dest.numberOfColumns != addend.numberOfColumns) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "dest.numberOfColumns != addend.numberOfColumns\n");
    exit(1);
  }

  if (dest.numberOfColumns != augend.numberOfColumns) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "dest.numberOfColumns != augend.numberOfColumns\n");
    exit(1);
  }

  if (augend.numberOfColumns != addend.numberOfColumns) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "augend.numberOfColumns != addend.numberOfColumns\n");
    exit(1);
  }

  if (dest.ordering != addend.ordering) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "dest.ordering != addend.ordering\n");
    exit(1);
  }

  if (dest.ordering != augend.ordering) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "dest.ordering != augend.ordering\n");
    exit(1);
  }

  if (augend.ordering != addend.ordering) {
    fprintf (stderr, "MatrixOfDouble_Add: "
             "augend.ordering != addend.ordering\n");
    exit(1);
  }

  for (i = 1; i <= dest.numberOfRows; i++) {
    for (j = 1; j <= dest.numberOfColumns; j++) {

      double Aij = MatrixOfDouble_GetEntry_1 (addend, i, j);
      double Bij = MatrixOfDouble_GetEntry_1 (augend, i, j);
      double Cij = Aij + Bij;

      MatrixOfDouble_SetEntry_1 (dest, i, j, Cij);

    }
  }
  
}







void             MatrixOfDouble_Sub (MatrixOfDouble dest,
                                     MatrixOfDouble minuend,
                                     MatrixOfDouble subtrahend) {

  unsigned int i = 0; /* Iteriert ueber die Zeilen von dest */
  unsigned int j = 0; /* Iteriert ueber die Spalten von dest */

  if (dest.values == NULL) {
    fprintf (stderr, "MatrixOfDouble_Sub: Result matrix is NULL\n");
    exit(1);
  }
  
  if (minuend.values == NULL) {
    fprintf (stderr, "MatrixOfDouble_Sub: Result matrix is NULL\n");
    exit(1);
  }
  
  if (subtrahend.values == NULL) {
    fprintf (stderr, "MatrixOfDouble_Sub: Result matrix is NULL\n");
    exit(1);
  }
   
  if (dest.numberOfRows != minuend.numberOfRows) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "dest.numberOfRows != minuend.numberOfRows\n");
    exit(1);
  }

  if (dest.numberOfRows != subtrahend.numberOfRows) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "dest.numberOfRows != subtrahend.numberOfRows\n");
    exit(1);
  }

  if (subtrahend.numberOfRows != minuend.numberOfRows) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "subtrahend.numberOfRows != minuend.numberOfRows\n");
    exit(1);
  }

  if (dest.numberOfColumns != minuend.numberOfColumns) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "dest.numberOfColumns != minuend.numberOfColumns\n");
    exit(1);
  }

  if (dest.numberOfColumns != subtrahend.numberOfColumns) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "dest.numberOfColumns != subtrahend.numberOfColumns\n");
    exit(1);
  }

  if (subtrahend.numberOfColumns != minuend.numberOfColumns) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "subtrahend.numberOfColumns != minuend.numberOfColumns\n");
    exit(1);
  }

  if (dest.ordering != minuend.ordering) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "dest.ordering != minuend.ordering\n");
    exit(1);
  }

  if (dest.ordering != subtrahend.ordering) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "dest.ordering != subtrahend.ordering\n");
    exit(1);
  }

  if (subtrahend.ordering != minuend.ordering) {
    fprintf (stderr, "MatrixOfDouble_Sub: "
             "subtrahend.ordering != minuend.ordering\n");
    exit(1);
  }

  for (i = 1; i <= dest.numberOfRows; i++) {
    for (j = 1; j <= dest.numberOfColumns; j++) {

      double Aij = MatrixOfDouble_GetEntry_1 (minuend, i, j);
      double Bij = MatrixOfDouble_GetEntry_1 (subtrahend, i, j);
      double Cij = Aij - Bij;

      MatrixOfDouble_SetEntry_1 (dest, i, j, Cij);

    }
  }
  
}







void             MatrixVectorMult_inPlace (VectorOfDouble y,
					   MatrixOfDouble A,
					   VectorOfDouble x) {

  /* Berechnet y = A * x, wobei der Vektor y bereits existiert,
     d.h. nicht mehr mit VectorOfDouble_Create (...) angelegt werden
     muss.
  */

  unsigned int i = 0; /* Iteriert ueber die Eintraege von y */
  unsigned int j = 0; /* Dient zur Berechnung eines Eintrags */

  if (A.values == NULL) {

    fprintf (stderr, "MatrixVectorMult_inPlace: Matrix is NULL\n");
    exit (1);

  }

  if (y.values == NULL) {

    fprintf (stderr, "MatrixVectorMult_inPlace: Result vector is NULL\n");
    exit (1);

  }

  if (x.values == NULL) {

    fprintf (stderr, "MatrixVectorMult_inPlace:"
	     " Vector to be multiplied is NULL\n");
    exit (1);

  }

  if (A.numberOfColumns != x.length) {

    fprintf (stderr, "MatrixVectorMult_inPlace: Incompatible dimensions"
	     " of vector and matrix\n");
    exit (1);

  }

  for (i = 1; i <= x.length; i++) {

    double entry = 0;

    for (j = 1; j <= A.numberOfColumns; j++) {

      entry = entry +
	MatrixOfDouble_GetEntry_1 (A,i,j) * VectorOfDouble_GetEntry_1 (x,j);

    }

    VectorOfDouble_SetEntry_1 (y, i, entry);

  }

}







VectorOfDouble   MatrixVectorMult (MatrixOfDouble A, VectorOfDouble x) {

  VectorOfDouble y;
  
  y = VectorOfDouble_Create (x.length);

  MatrixVectorMult_inPlace (y, A, x);

  return y;

}







void             MatrixMatrixMult_inPlace (MatrixOfDouble C,
                                           MatrixOfDouble A,
                                           MatrixOfDouble B) {

  /* Berechnet C = A * B, wobei die Matrix C bereits existiert,
     d.h. nicht mehr mit MatrixOfDouble_Create (...) angelegt werden
     muss.
  */

  unsigned int i = 0; /* Iteriert ueber die Zeilen von C */
  unsigned int j = 0; /* Iteriert ueber die Spalten von C */

  if (A.values == NULL) {
    fprintf (stderr, "MatrixMatrixMult_inPlace: "
             "Left-hand matrix is NULL\n");
    exit (1);
  }

  if (B.values == NULL) {
    fprintf (stderr, "MatrixMatrixMult_inPlace: "
             "Right-hand matrix is NULL\n");
    exit (1);
  }

  if (C.values == NULL) {
    fprintf (stderr, "MatrixMatrixMult_inPlace: "
             "Result matrix is NULL\n");
    exit (1);
  }

  if (A.numberOfColumns != B.numberOfRows) {
    fprintf (stderr, "MatrixMatrixMult_inPlace (C, A, B): "
             "# of rows of B is not equal to # of columns of A\n");
    exit(1);
  }

  if (C.numberOfRows != A.numberOfRows) {
    fprintf (stderr, "MatrixMatrixMult_inPlace (C, A, B): "
             "# of rows of C is not equal to # of rows of A\n");
    exit(1);
  }

  if (C.numberOfColumns != B.numberOfColumns) {
    fprintf (stderr, "MatrixMatrixMult_inPlace (C, A, B): "
             "# of columns of C is not equal to # of columns of B\n");
    exit(1);
  }

  for (i = 1; i <= C.numberOfRows; i++) {
    for (j = 1; j <= C.numberOfColumns; j++) {

      /* Eintrag C[i,j] berechnen */

      unsigned int k = 0;
      double entry = 0;

      for (k = 1; k <= A.numberOfColumns; k++) {

        double Aik = MatrixOfDouble_GetEntry_1 (A, i, k);
        double Bkj = MatrixOfDouble_GetEntry_1 (B, k, j);

        entry += Aik * Bkj;

      }
      
      MatrixOfDouble_SetEntry_1 (C, i, j, entry);

    } /* Spalten von C durchgehen */
  } /* Zeilen von C durchgehen */
}







MatrixOfDouble   MatrixMatrixMult (MatrixOfDouble A,
                                   MatrixOfDouble B) {

  MatrixOfDouble C;

  C = MatrixOfDouble_Create (A.numberOfRows,
                             B.numberOfColumns,
                             ROWWISE);

  MatrixMatrixMult_inPlace (C, A, B);

  return C;

}







void             VectorIteration (MatrixOfDouble A,
				  VectorOfDouble v,
				  unsigned int max) {

  unsigned int i;
  VectorOfDouble dummy;

  /* Zuerst die Fehlerbehandlung: ein Aufruf von
     MatrixVectorMult_inPlace nimmt uns viel Arbeit ab! */

  dummy = VectorOfDouble_Create (v.length);

  MatrixVectorMult_inPlace (dummy, A, v);

  VectorOfDouble_Destroy (&dummy);

  /* Wenn die Programmausfuehrung bis hierhin ueberlebt, passen
     die Matrix und der Vektor zueinander. */

  for (i = 1; i <= max; i++) {

    v = MatrixVectorMult (A, v);

    /* VectorOfDouble_Normalise (v); */

    /* if (i >= max - 5) { */

    /*   printf ("s%u = ",i); VectorOfDouble_Print (v); printf ("\n"); */

    /* } */

  }

}







void             NormalisedVectorIteration (MatrixOfDouble A,
					    VectorOfDouble v,
					    unsigned int max) {

  unsigned int i;
  VectorOfDouble dummy;

  /* Zuerst die Fehlerbehandlung: ein Aufruf von
     MatrixVectorMult_inPlace nimmt uns viel Arbeit ab! */

  dummy = VectorOfDouble_Create (v.length);

  MatrixVectorMult_inPlace (dummy, A, v);

  VectorOfDouble_Destroy (&dummy);

  /* Wenn die Programmausfuehrung bis hierhin ueberlebt, passen
     die Matrix und der Vektor zueinander. */

  for (i = 1; i <= max; i++) {

    v = MatrixVectorMult (A, v);

    VectorOfDouble_Normalise (v);

    /* if (i >= max - 5) { */

    /*   printf ("s%u = ",i); VectorOfDouble_Print (v); printf ("\n"); */

    /* } */

  }

}

