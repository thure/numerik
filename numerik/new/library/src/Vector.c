#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "Tuple.h"
#include "Vector.h"

/* Diese Funktion steht nicht in der .h-Datei, denn sie soll nicht von
   ausserhalb dieser .c-Datei aufgerufen werden */

void           VectorOfDouble_ReallyDestroy (VectorOfDouble* vp);

/*************************************************************************/

VectorOfDouble VectorOfDouble_Create  (unsigned int length) {

  /* Bekommt eine natuerliche Zahl n uebergeben und bastelt einen
     Vektor der Laenge n. Hier ist ein Vektor ein Paar bestehend aus
     einem Tupel und dessen Laenge. Alle Eintraege werden mit -1
     vorbesetzt.
  */

  VectorOfDouble result;

  result.length = length;
  result.values = TupleOfDouble_Create (length);
  

  if (length == 0) {

    fprintf (stderr, "Unable to create vector: Tuple without components\n");

  }

  if (result.values == NULL) {

    fprintf (stderr, "Unable to create vector: tuple is NULL\n");
    
  }

  return result;

}








void             VectorOfDouble_CopyValues  (VectorOfDouble dest,
					     VectorOfDouble src) {

  /* Performs dest = src, component-wise. */

  unsigned int k;

  if (src.values == NULL) {

    fprintf (stderr, "VectorOfDouble_CopyValues"
	     ": Vector to copy is NULL\n");
    exit (1);

  }

  if (dest.values == NULL) {

    fprintf (stderr, "VectorOfDouble_CopyValues"
	     ": Vector to hold result is NULL\n");
    exit (1);

  }

  if (src.length != dest.length) {

    fprintf (stderr, "VectorOfDouble_CopyValues:"
	     " Vectors are not of equal length\n");
    exit (1);

  }

  for (k = 1; k <= src.length; k++) {

    VectorOfDouble_SetEntry_1 (dest, k, VectorOfDouble_GetEntry_1(src, k));

  }

}







VectorOfDouble   VectorOfDouble_Copy  (VectorOfDouble src) {

  VectorOfDouble dest;

  if (src.values == NULL) {

    fprintf (stderr, "VectorOfDouble_Copy: Vector to copy is NULL\n");
    exit (1);

  }
  
  dest = VectorOfDouble_Create (src.length);

  VectorOfDouble_CopyValues (dest, src);

  return dest;

}







void           VectorOfDouble_Destroy (VectorOfDouble* vp) {

  /* Gibt den Speicher wieder frei, den der Vektor v belegt. */
  
  /* v ist eine Instanz der Struktur VectorOfDouble, kann also nicht
     direkt modifiziert werden. Also uebergeben wir eine Referenz auf v
     an eine Hilfsfunktion. */

  /* VectorOfDouble_ReallyDestroy (&v); */

  
  TupleOfDouble_Destroy (vp->values);
  vp->values = NULL;

}







void           VectorOfDouble_ReallyDestroy (VectorOfDouble* vp) {

  vp->length = 0;
  TupleOfDouble_Destroy (vp->values);
  vp->values = NULL;
  fprintf (stderr, "DESTROY: Vektor.values zeigt auf %p\n\n",
           (void*)vp->values);
  
}







VectorOfDouble   VectorOfDouble_Create_Sin  (unsigned int N,
                                             unsigned int mu) {

  /* Erstellt einen speziellen Vektor mit Sinuseintraegen */

  /* Siehe EMMI-Kurs im Maerz 2012 */

  /* N ist die Anzahl der Stuetzstellen */
  /* mu ist ein weiterer Parameter... */
  /* todo: Dokumentieren.*/

  double h;
  VectorOfDouble s;
  unsigned int VectorLength;
  double sinSample;
  unsigned int i;
  const double PI = 4 * atan (1);

  if (N <= 1) {

    fprintf (stderr, "VectorOfDouble_Create_Sin:"
	     " Too few sampling points\n");
    exit (1);

  }

  if (mu > N-1) {

    fprintf (stderr, "VectorOfDouble_Create_Sin:"
	     " mu is too large\n");
    exit (1);

  }

  h = 1.0 / N;
  VectorLength = N-1;

  s = VectorOfDouble_Create (VectorLength);

  for (i = 1; i <= VectorLength; i++) {

    sinSample = sin (mu * PI * i * h);

    VectorOfDouble_SetEntry_1 (s, i, sinSample);

  }

  return s;

}







void           VectorOfDouble_Print (VectorOfDouble v) {

  /* TODO
     Zahlenformat flexibel gestalten
   */
  
  unsigned int k;

  if (v.values == NULL) {

    fprintf (stderr, "VectorOfDouble_Print: Nonexistent vector\n");
    exit (1);

  }

  printf ("[");
  
  for (k = 1; k < v.length; k++) {

    printf ("% 3.8f,  ", VectorOfDouble_GetEntry_1(v,k));

  }

  printf ("% 3.8f]", VectorOfDouble_GetEntry_1(v,v.length));
  
}







double           VectorOfDouble_GetEntry_0 (VectorOfDouble v,
                                            unsigned int i) {

  if (i+1 > v.length) {

    fprintf (stderr, "VectorOfDouble_GetEntry: Index out of bounds\n");
    exit (1);

  }

  if (v.values == NULL) {

    fprintf (stderr, "VectorOfDouble_GetEntry: Nonexistent vector\n");
    exit (1);

  }

  return v.values[i];

}







double           VectorOfDouble_GetEntry_1 (VectorOfDouble v,
                                            unsigned int i) {

  /* if (i == 0) { */
  /*   /\* dieser Fall muss extra geprueft werden, weil i ein unsigned int */
  /*      ist und -1 in der Bineardarstellung aus lauter Einsen
	  besteht. Das i von hier, das hier den Wert 0 hat, wird um 1
	  vermindert, hat als unsigned int also einen sehr hohen
	  Wert. */
  /*    *\/ */

  /*   fprintf (stderr, "VectorOfDouble_GetEntry: Index out of bounds\n"); */
  /*   exit (1); */

  /* } */

  return VectorOfDouble_GetEntry_0 (v, i-1);

}







void             VectorOfDouble_SetEntry_0 (VectorOfDouble v,
                                            unsigned int i,
                                            double x) {

  if (i + 1 > v.length) {

    fprintf (stderr, "VectorOfDouble_SetEntry: Index out of bounds\n");
    exit (1);

  }

  if (v.values == NULL) {

    fprintf (stderr, "VectorOfDouble_SetEntry: Nonexistent vector\n");
    exit (1);

  }

  v.values[i] = x;

}







void             VectorOfDouble_SetEntry_1 (VectorOfDouble v,
                                            unsigned int i,
                                            double x) {

  VectorOfDouble_SetEntry_0 (v, i-1, x);

}







void             VectorOfDouble_Add (VectorOfDouble dest,
				     VectorOfDouble addend,
				     VectorOfDouble augend) {

  /* Hier fehlt Fehlerbehandlung */

  unsigned int i = 0;

  for (i = 1; i <= dest.length; i++) {

    double addend_i = VectorOfDouble_GetEntry_1 (addend, i);
    double augend_i = VectorOfDouble_GetEntry_1 (augend, i);

    VectorOfDouble_SetEntry_1 (dest, i, addend_i + augend_i);

  }
  
}







void             VectorOfDouble_Sub (VectorOfDouble dest,
				     VectorOfDouble minuend,
				     VectorOfDouble subtrahend) {

  /* Hier fehlt Fehlerbehandlung */

  unsigned int i = 0;

  for (i = 1; i <= dest.length; i++) {

    double minuend_i    = VectorOfDouble_GetEntry_1 (minuend, i);
    double subtrahend_i = VectorOfDouble_GetEntry_1 (subtrahend, i);

    VectorOfDouble_SetEntry_1 (dest, i, minuend_i - subtrahend_i);

  }

}







double           VectorOfDouble_ScalarProduct (VectorOfDouble u,
					       VectorOfDouble v) {

  /* Standardskalarprodukt. */

  double result;
  unsigned int k;

  if (u.values == NULL || v.values == NULL) {

    fprintf (stderr, "VectorOfDouble_ScalarProduct:"
	     " Vector to be multiplied is NULL\n");
    exit (1);

  }

  if (u.length != v.length) {

    fprintf (stderr, "VectorOfDouble_ScalarProduct:"
	     " Vectors are not of the same length\n");
    exit (1);

  }

  result = 0;

  for (k = 1; k <= u.length; k++) {

    result = result +
      VectorOfDouble_GetEntry_1(u, k) * VectorOfDouble_GetEntry_1(v, k);

  }

  return result;

}







double           VectorOfDouble_Norm (VectorOfDouble v) {

  /* Aus dem Standardskalarprodukt abgeleitete Norm. */

  /* || v  || = sqrt (< x, x >) */

  return sqrt(VectorOfDouble_ScalarProduct(v, v));

}







void             VectorOfDouble_Scale (VectorOfDouble v, double s) {

  /* Streckt/staucht einen Vektor. */
  unsigned int k;

  if (v.values == NULL) {

    fprintf (stderr, "VectorOfDouble_Scale: Nonexistent vector\n");
    exit (1);

  }

  for (k = 1; k <= v.length; k++) {

    VectorOfDouble_SetEntry_1 (v, k, s * VectorOfDouble_GetEntry_1 (v, k));

  }

}







void             VectorOfDouble_Normalise (VectorOfDouble v) {

  /* Normiert einen Vektor auf Norm 1. */
  double n;

  if (v.values == NULL) {

    fprintf (stderr, "VectorOfDouble_Normalise: Nonexistent vector\n");
    exit (1);

  }

  n = VectorOfDouble_Norm(v);
  
  if (n <= 0.00000001) {

    fprintf (stderr, "VectorOfDouble_Normalise: Scalar product is zero\n");
    exit (1);

  }

  VectorOfDouble_Scale (v, 1/n);

}
