#include <stdlib.h>
#include <stdio.h>
#include "Tuple.h"

TupleOfDouble TupleOfDouble_Create  (unsigned int length) {

  /* Bekommt eine natuerliche Zahl n uebergeben, fordert ausreichend
     Speicher an, um n Double-Werte zusammenhaengend aufzunehmen,
     initialisiert alle Komponenten mit -1, und gibt bei Erfolg die
     Adresse zurueck, an dem der Speicherblock beginnt, bei Misserfolg
     wird ein NULL-Zeiger zurueckgegeben.     
  */

  TupleOfDouble tuple = NULL;
  unsigned int i;

  if (length == 0) {

    fprintf (stderr, "Invalid initialiser: Tuple without components\n");
    return NULL;

  }

  tuple = (TupleOfDouble) malloc(length * sizeof (double));

  if (tuple == NULL) {

    fprintf (stderr, "Unable to create tuple\n");
    return NULL;
  }

  for (i = 0; i < length; i++) tuple[i] = -1;

  return tuple;

}







void          TupleOfDouble_Destroy (TupleOfDouble t) {

  /* Gibt den Speicher wieder frei, den das Tupel t belegt. */

  free (t);
}
