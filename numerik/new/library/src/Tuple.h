#ifndef MY_HEADER_TUPLE

#define MY_HEADER_TUPLE

typedef double*  TupleOfDouble;





/*
  Erzeugt ein Tupel vorgegebener Laenge.
 */
TupleOfDouble    TupleOfDouble_Create  (unsigned int length);





/*
  Gibt den Speicher wieder frei, den das Tupel belegt.
 */
void             TupleOfDouble_Destroy (TupleOfDouble t);

#endif
