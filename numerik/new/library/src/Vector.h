#ifndef MY_HEADER_VECTOR

#define MY_HEADER_VECTOR

#include "Tuple.h"



struct tmp_VectorOfDouble {

  unsigned int    length; /* Laenge des Vektors, > 0 */

  TupleOfDouble   values; /* Eintraege des Vektors   */

};

typedef struct tmp_VectorOfDouble VectorOfDouble;





/*
  Erzeugt einen neuen Vektor mit vorgegebener Laenge.
 */
VectorOfDouble   VectorOfDouble_Create  (unsigned int length);





/*
  Kopiert die Eintraege eines Vektors in einen anderen, bereits
  bestehenden Vektor gleicher Laenge.
 */
void             VectorOfDouble_CopyValues  (VectorOfDouble dest,
                                             VectorOfDouble src);




/*
  Erzeugt einen neuen Vektor der Laenge eines bereits bestehenden
  Vektors und kopiert die Eintraege aus letzterem in jenen.
 */
VectorOfDouble   VectorOfDouble_Copy  (VectorOfDouble src);




/*
  Gibt den Speicher wieder frei, den der Vektor belegt.
  Soll ausserdem den Zeiger auf die Eintraege auf NULL setzen, aber
  das scheint noch nicht zu funktionieren.
 */
void             VectorOfDouble_Destroy (VectorOfDouble* vp);





/*
  Erzeugt einen speziellen Vektor mit Sinuseintraegen.
  Siehe EMMI-Kurs im Maerz 2012.
 */
VectorOfDouble   VectorOfDouble_Create_Sin  (unsigned int N,
                                             unsigned int mu);





/*
  Gibt einen Vektor auf der Standardausgabe menschenlesbar aus.
 */
void             VectorOfDouble_Print (VectorOfDouble v);




/*
  Liefert den Eintrag der i-ten Komponente, wobei Komponenten
  mit Null beginnend gezaehlt werden.
 */
double           VectorOfDouble_GetEntry_0 (VectorOfDouble v,
                                            unsigned int i);




/*
  Liefert den Eintrag der i-ten Komponente, wobei Komponenten
  mit Eins beginnend gezaehlt werden.
 */
double           VectorOfDouble_GetEntry_1 (VectorOfDouble v,
                                            unsigned int i);




/*
  Setzt den Eintrag der i-ten Komponente, wobei Komponenten
  mit Null beginnend gezaehlt werden.
 */
void             VectorOfDouble_SetEntry_0 (VectorOfDouble v,
                                            unsigned int i,
                                            double x);




/*
  Setzt den Eintrag der i-ten Komponente, wobei Komponenten
  mit Eins beginnend gezaehlt werden.
 */
void             VectorOfDouble_SetEntry_1 (VectorOfDouble v,
                                            unsigned int i,
                                            double x);




/*
  Addiert die Vektoren addend und augend und speichert das Ergebnis im
  Vektor dest.
 */
void             VectorOfDouble_Add (VectorOfDouble dest,
				     VectorOfDouble addend,
				     VectorOfDouble augend);




/*
  Subtrahiert den Vektor subtrahend vom Vektor minuend
  und speichert das Ergebnis im Vektor dest.
 */
void             VectorOfDouble_Sub (VectorOfDouble dest,
				     VectorOfDouble minuend,
				     VectorOfDouble subtrahend);




/*
  Berechnet das Standardskalarprodukt der Vektoren u und v.
 */
double           VectorOfDouble_ScalarProduct (VectorOfDouble u,
					       VectorOfDouble v);




/*
  Berechnet die aus dem Standardskalarprodukt abgeleitete Norm des
  Vektors v.
 */
double           VectorOfDouble_Norm (VectorOfDouble v);




/*
  Multipliziert jede Komponente von v mit s.
 */
void             VectorOfDouble_Scale (VectorOfDouble v, double s);




/*
  Normiert den Vektor v auf Norm 1.
 */
void             VectorOfDouble_Normalise (VectorOfDouble v);




/* noch weitere Operationen auf Vektoren definieren? */
/* Vektoreintraege im laufenden Programm eingeben */
/* im "double"^3 ein Kreuzprodukt berechnen? */
/* vielleicht auch weitere Skalarprodukte und Normen?
   Auch solche Normen, die nicht durch ein Skalarprodukt induziert sind ?*/
/* man koennte auch von einem Vektor der Laenge a und einem der Laenge
   b die Komponenten "aneinanderhaengen" und so einen Vektor der
   Laenge a+b erhalten */

#endif
