#ifndef MY_HEADER_MATRIX

#define MY_HEADER_MATRIX

#include "Tuple.h"
#include "Vector.h"



enum tmp_MatrixOrdering { NONE, ROWWISE, COLUMNWISE };

typedef enum tmp_MatrixOrdering MatrixOrdering;




struct tmp_MatrixOfDouble {
    
  unsigned int   numberOfRows;    /* Anzahl der Zeilen,  > 0 */

  unsigned int   numberOfColumns; /* Anzahl der Spalten, > 0 */

  TupleOfDouble  values;          /* Eintraege der Matrix */

  MatrixOrdering ordering;        /* Zeilen- oder spaltenweise
				     Anordnung der Elemente im
				     Speicher */
};

typedef struct tmp_MatrixOfDouble MatrixOfDouble;





/*
  Erzeugt eine neue Matrix mit vorgegebener Zeilen- und Spaltenanzahl und Anordnung
  der Elemente im Speicher.
 */
MatrixOfDouble   MatrixOfDouble_Create  (unsigned int rows,
                                         unsigned int cols,
					 MatrixOrdering ordering);





/*
  Kopiert die Eintraege einer Matrix in eine andere, bereits
  bestehende Matrix gleicher Zeilen- und Spaltenanzahl und Anordnung.
 */
void             MatrixOfDouble_CopyValues  (MatrixOfDouble dest,
                                             MatrixOfDouble src);





/*
  Erzeugt eine neue Matrix mit Zeilen- und Spaltenanzahl und Anordnung
  einer bereits bestehenden Matrix und kopiert die Eintraege aus
  letzterer in jene.
 */
MatrixOfDouble   MatrixOfDouble_Copy (MatrixOfDouble src);





/*
  Gibt den Speicher wieder frei, den die Matrix belegt.
  Soll ausserdem den Zeiger auf die Eintraege auf NULL setzen, aber
  das scheint noch nicht zu funktionieren.
 */
void             MatrixOfDouble_Destroy (MatrixOfDouble A);





/*
  Liest eine Matrix aus einer Textdatei vorgegebenen Formats.
 */
MatrixOfDouble   MatrixOfDouble_LoadFromFile (const char* filename);





/*
  Gibt eine Matrix auf der Standardausgabe menschenlesbar aus.
 */
void             MatrixOfDouble_Print (MatrixOfDouble A);





/*
  Gibt die Anordnung der Elemente im Speicher zurueck.
 */
MatrixOrdering   MatrixOfDouble_getOrdering (MatrixOfDouble A);





/*
  Legt die Anordnung der Elemente im Speicher fest.
 */
void             MatrixOfDouble_setOrdering (MatrixOfDouble A,
                                             MatrixOrdering ordering);





/*
  Gibt die Anordnung der Elemente im Speicher auf der Standardausgabe
  menschenlesbar aus.
 */
void             MatrixOfDouble_PrintOrdering (MatrixOfDouble A);





/*
  Liefert den Eintrag an der i-ten Zeile der j-ten Spalte, wobei
  Zeilen und Spalten mit Null beginnend gezaehlt werden.
 */
double           MatrixOfDouble_GetEntry_0 (MatrixOfDouble A,
                                            unsigned int i,
                                            unsigned int j);





/*
  Liefert den Eintrag an der i-ten Zeile der j-ten Spalte, wobei
  Zeilen und Spalten mit Eins beginnend gezaehlt werden.
 */
double           MatrixOfDouble_GetEntry_1 (MatrixOfDouble A,
                                            unsigned int i,
                                            unsigned int j);





/*
  Setzt den Eintrag an der i-ten Zeile der j-ten Spalte, wobei
  Zeilen und Spalten mit Null beginnend gezaehlt werden.
 */
void             MatrixOfDouble_SetEntry_0 (MatrixOfDouble A,
                                            unsigned int i,
                                            unsigned int j,
                                            double x);





/*
  Setzt den Eintrag an der i-ten Zeile der j-ten Spalte, wobei
  Zeilen und Spalten mit Eins beginnend gezaehlt werden.
 */
void             MatrixOfDouble_SetEntry_1 (MatrixOfDouble A,
                                            unsigned int i,
                                            unsigned int j,
                                            double x);





/*
  Addiert die Matrizen addend und augend und speichert das Ergebnis
  in der Matrix dest.
 */
void             MatrixOfDouble_Add (MatrixOfDouble dest,
                                     MatrixOfDouble addend,
                                     MatrixOfDouble augend);





/*
  Subtrahiert die Matrix subtrahend von der Matrix minuend
  und speichert das Ergebnis in der Matrix dest.
 */
void             MatrixOfDouble_Sub (MatrixOfDouble dest,
                                     MatrixOfDouble minuend,
                                     MatrixOfDouble subtrahend);





/*
  Berechnet das Matrix-Vektor-Produkt A * x und speichert das Ergebnis
  im bereits existierenden Vektor y.
 */
void             MatrixVectorMult_inPlace (VectorOfDouble y,
                                           MatrixOfDouble A,
                                           VectorOfDouble x);





/*
  Berechnet das Matrix-Vektor-Produkt A * x und liefert einen neuen
  Vektor mit dem Ergebnis zurueck.
 */
VectorOfDouble   MatrixVectorMult (MatrixOfDouble A, VectorOfDouble x);





/*
  Berechnet das Matrix-Matrix-Produkt A * B und speichert das Ergebnis
  in der bereits existierenden Matrix C.
 */
void             MatrixMatrixMult_inPlace (MatrixOfDouble C,
                                           MatrixOfDouble A,
                                           MatrixOfDouble B);





/*
  Berechnet das Matrix-Matrix-Produkt A * B und liefert eine neue
  Matrix mit dem Ergebnis zurueck.
 */
MatrixOfDouble   MatrixMatrixMult (MatrixOfDouble A,
                                   MatrixOfDouble B);





/*
  Ueberschreibt den Vektor v mit dem Matrix-Vektor-Produkt A * v.
 */
void             VectorIteration (MatrixOfDouble A,
				  VectorOfDouble v,
				  unsigned int max);





/*
  Ueberschreibt den Vektor v mit dem Matrix-Vektor-Produkt A * v und
  normiert v danach auf Norm 1.
 */
void             NormalisedVectorIteration (MatrixOfDouble A,
					    VectorOfDouble v,
					    unsigned int max);

#endif
